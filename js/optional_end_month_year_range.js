/**
 * @file
 */
(function ($, Drupal, drupalSettings) {
  'use strict';
  Drupal.behaviors.optional_end_month_year_range = {
    attach: function (context, settings) {
      function endValueWrapper(item, tag = '') {
        var wrapper = '.end-value-wrapper';
        if (tag) {
          wrapper += ' ' + tag;
        }
        return $(item).parent().parent().find(wrapper);
      }
      if ($('input.no-end-date-form-checkbox').length) {
        $('input.no-end-date-form-checkbox', context).each(function () {
          if ($(this).is(':checked')) {
            endValueWrapper(this).hide();
          }
        }).on('click', function () {
          if ($(this).is(':checked')) {
            endValueWrapper(this).hide();
            endValueWrapper(this, 'select').prop('selectedIndex', 0);
          }
          else {
            endValueWrapper(this).show();
          }
        });
      }
    }
  };
})(jQuery, Drupal, drupalSettings);
