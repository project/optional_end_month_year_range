<?php

namespace Drupal\optional_end_month_year_range;

use Drupal\Core\Field\FieldItemListInterface;

/**
 * Provides friendly methods for datetime range.
 */
trait OptionalEndMonthYearRangeTrait {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $separator = $this->getSetting('separator');

    foreach ($items as $delta => $item) {
      if (!empty($item->start_date)) {
        /** @var \Drupal\Core\Datetime\DrupalDateTime $start_date */
        $start_date = $item->start_date;

        if (!empty($item->end_date)) {
          /** @var \Drupal\Core\Datetime\DrupalDateTime $end_date */
          $end_date = $item->end_date;

          if ($start_date->getTimestamp() !== $end_date->getTimestamp()) {
            $elements[$delta] = [
              'start_date' => $this->buildDateWithIsoAttribute($start_date),
              'separator' => ['#plain_text' => ' ' . $separator . ' '],
              'end_date' => $this->buildDateWithIsoAttribute($end_date),
            ];
          }
          else {
            $elements[$delta] = [
              'start_date' => $this->buildDateWithIsoAttribute($start_date),
            ];
          }
        }
        else {
          $elements[$delta] = [
            'start_date' => $this->buildDateWithIsoAttribute($start_date),
          ];
        }
      }
    }

    return $elements;
  }

}
