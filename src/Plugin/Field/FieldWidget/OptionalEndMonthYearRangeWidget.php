<?php

namespace Drupal\optional_end_month_year_range\Plugin\Field\FieldWidget;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\datetime_range\Plugin\Field\FieldWidget\DateRangeDatelistWidget;

/**
 * Plugin implementation of the 'month_year_range' widget.
 *
 * @FieldWidget(
 *   id = "optional_end_month_year_range",
 *   label = @Translation("Optional Month Year range"),
 *   field_types = {
 *     "optional_end_month_year_range"
 *   }
 * )
 */
class OptionalEndMonthYearRangeWidget extends DateRangeDatelistWidget implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'date_order' => 'YM',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $date_order = $this->getSetting('date_order');

    // Set up the date part order array.
    switch ($date_order) {
      default:
      case 'YMD':
        $date_part_order = ['year', 'month', 'day'];
        break;

      case 'MDY':
        $date_part_order = ['month', 'day', 'year'];
        break;

      case 'DMY':
        $date_part_order = ['day', 'month', 'year'];
        break;

      case 'YM':
        $date_part_order = ['year', 'month'];
        break;

      case 'MY':
        $date_part_order = ['month', 'year'];
        break;

      case 'Y':
        $date_part_order = ['year'];
        break;
    }

    $element['value'] = [
      '#type' => 'datelist',
      '#prefix' => '<div class="' . self::setClass($items, $element, 'start-value-wrapper') . '">',
      '#suffix' => '</div>',
      '#date_part_order' => $date_part_order,
    ] + $element['value'];

    $element['end_value'] = [
      '#type' => 'datelist',
      '#prefix' => '<div class="' . self::setClass($items, $element, 'end-value-wrapper') . '">',
      '#suffix' => '</div>',
      '#date_part_order' => $date_part_order,
      '#required' => FALSE,
    ] + $element['end_value'];

    $no_end_date_text = $this->getFieldSetting('no_end_date_label');
    $element['no_end_date'] = [
      '#type' => 'checkbox',
      '#default_value' => $items[$delta]->no_end_date,
      '#title' => $no_end_date_text ? $this->t('@value', ['@value' => $no_end_date_text]) : $this->t('No end date'),
      '#attributes' => ['class' => ['no-end-date-form-checkbox']],
    ];

    $element['#element_validate'][] = [$this, 'validateOptionalNoEndDate'];

    // The '#states' does not work correctly in 'datelist' yet.
    // So we are using an alternative solution by js.
    $element['#attached']['library'][] = 'optional_end_month_year_range/optional_end_month_year_range';

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);
    $element['date_order'] = [
      '#type' => 'select',
      '#title' => $this->t('Date part order'),
      '#default_value' => $this->getSetting('date_order'),
      '#options' => [
        'MDY' => $this->t('Month/Day/Year'),
        'DMY' => $this->t('Day/Month/Year'),
        'YMD' => $this->t('Year/Month/Day'),
        'YM' => $this->t('Year/Month'),
        'MY' => $this->t('Month/Year'),
        'Y' => $this->t('Year'),
      ],
    ];

    return $element;
  }

  /**
   * Add class value.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The field values to be rendered.
   * @param array $element
   *   An associative array containing the properties and children of the
   *   generic form element.
   * @param string $class_name
   *   The class name.
   *
   * @return string
   *   The class value.
   */
  public static function setClass(FieldItemListInterface $items, array $element, $class_name) {
    $class = !empty($element['#field_parents']) ? (strtr(implode($element['#field_parents'], '-') . '-' . $items->getName(), '_', '-') . '-' . $class_name) : '';
    $class .= ' ';
    $class .= strtr($items->getName(), '_', '-') . '-' . $class_name;
    $class .= ' ';
    $class .= $class_name;

    return $class;
  }

  /**
   * Ensure that the end date is not null if no_end_date field is not checked.
   *
   * @param array $element
   *   An associative array containing the properties and children of the
   *   generic form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   */
  public function validateOptionalNoEndDate(array &$element, FormStateInterface $form_state, array &$complete_form) {
    $start_date = $element['value']['#value']['object'];
    $end_date = $element['end_value']['#value']['object'];
    $no_end_date = $element['no_end_date']['#value'];

    if ($start_date instanceof DrupalDateTime && !($end_date instanceof DrupalDateTime) && !$no_end_date) {
      $form_state->setError($element, $this->t('This value should not be null.'));
    }
  }

}
