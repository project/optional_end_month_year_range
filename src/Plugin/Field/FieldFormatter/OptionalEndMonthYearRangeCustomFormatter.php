<?php

namespace Drupal\optional_end_month_year_range\Plugin\Field\FieldFormatter;

use Drupal\optional_end_month_year_range\OptionalEndMonthYearRangeTrait;
use Drupal\datetime_range\Plugin\Field\FieldFormatter\DateRangeCustomFormatter;

/**
 * Plugin implementation of the 'Custom' formatter.
 *
 * This formatter renders the data range as plain text, with a fully
 * configurable date format using the PHP date syntax and separator.
 *
 * @FieldFormatter(
 *   id = "optional_end_month_year_range_custom",
 *   label = @Translation("Custom"),
 *   field_types = {
 *     "optional_end_month_year_range"
 *   }
 * )
 */
class OptionalEndMonthYearRangeCustomFormatter extends DateRangeCustomFormatter {

  use OptionalEndMonthYearRangeTrait;

}
