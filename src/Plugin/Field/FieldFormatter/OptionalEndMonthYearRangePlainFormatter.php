<?php

namespace Drupal\optional_end_month_year_range\Plugin\Field\FieldFormatter;

use Drupal\optional_end_month_year_range\OptionalEndMonthYearRangeTrait;
use Drupal\datetime_range\Plugin\Field\FieldFormatter\DateRangePlainFormatter;

/**
 * Plugin implementation of the 'Plain' formatter.
 *
 * This formatter renders the data range as a plain text string, with a
 * configurable separator using an ISO-like date format string.
 *
 * @FieldFormatter(
 *   id = "optional_end_month_year_range_plain",
 *   label = @Translation("Plain"),
 *   field_types = {
 *     "optional_end_month_year_range"
 *   }
 * )
 */
class OptionalEndMonthYearRangePlainFormatter extends DateRangePlainFormatter {

  use OptionalEndMonthYearRangeTrait;

}
