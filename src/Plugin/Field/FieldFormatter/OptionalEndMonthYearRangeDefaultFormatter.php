<?php

namespace Drupal\optional_end_month_year_range\Plugin\Field\FieldFormatter;

use Drupal\optional_end_month_year_range\OptionalEndMonthYearRangeTrait;
use Drupal\datetime_range\Plugin\Field\FieldFormatter\DateRangeDefaultFormatter;

/**
 * Plugin implementation of the 'Default' formatter.
 *
 * This formatter renders the data range using <time> elements, with
 * configurable date formats (from the list of configured formats) and a
 * separator.
 *
 * @FieldFormatter(
 *   id = "optional_end_month_year_range_default",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "optional_end_month_year_range"
 *   }
 * )
 */
class OptionalEndMonthYearRangeDefaultFormatter extends DateRangeDefaultFormatter {

  use OptionalEndMonthYearRangeTrait;

}
