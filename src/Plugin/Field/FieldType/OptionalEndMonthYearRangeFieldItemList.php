<?php

namespace Drupal\optional_end_month_year_range\Plugin\Field\FieldType;

use Drupal\datetime_range\Plugin\Field\FieldType\DateRangeFieldItemList;

/**
 * Represents a configurable entity daterange field.
 */
class OptionalEndMonthYearRangeFieldItemList extends DateRangeFieldItemList {

}
