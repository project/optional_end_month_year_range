<?php

namespace Drupal\optional_end_month_year_range\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\datetime_range\Plugin\Field\FieldType\DateRangeItem;

/**
 * Plugin implementation of the 'daterange' field type.
 *
 * @FieldType(
 *   id = "optional_end_month_year_range",
 *   label = @Translation("Optional End Month Year Range"),
 *   description = @Translation("Create and store date ranges."),
 *   default_widget = "optional_end_month_year_range",
 *   default_formatter = "optional_end_month_year_range_default",
 *   list_class = "\Drupal\optional_end_month_year_range\Plugin\Field\FieldType\OptionalEndMonthYearRangeFieldItemList"
 * )
 */
class OptionalEndMonthYearRangeItem extends DateRangeItem {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);

    $schema['columns']['value']['description'] = 'The start date value.';

    $schema['columns']['end_value'] = [
      'description' => 'The end date value.',
    ] + $schema['columns']['value'];

    $schema['indexes']['end_value'] = ['end_value'];

    $schema['columns']['no_end_date'] = [
      'description' => 'No end Date',
      'type' => 'int',
      'size' => 'tiny',
      'not null' => FALSE,
    ];

    $schema['indexes']['no_end_date'] = ['no_end_date'];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);
    $properties['end_value']->setRequired(FALSE);
    $properties['no_end_date'] = DataDefinition::create('boolean')
      ->setLabel(t('Boolean value'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'no_end_date_label' => "No end date",
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = parent::storageSettingsForm($form, $form_state, $has_data);

    $element['no_end_date_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('No end date text'),
      '#default_value' => $this->getSetting('no_end_date_label'),
    ];

    return $element;
  }

}
