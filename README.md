INTRODUCTION
------------

The Optional End Month Year Range module is based on DateRange module.
It creates a new field based on DateRange
with a checkbox that makes the end date optional as per user choice.
You can change the title of the "No end date" checkbox
in the "Field settings" field.

REQUIREMENTS
------------

This module requires the following core modules:
 * datetime
 * datetime_range

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

 * No specific configuration is needed.

MAINTAINERS
-----------

Current maintainers:
 * Ornella RABE (harinjaka) - https://www.drupal.org/user/3655518
 * Jean Nica Ralava (nikral) - https://www.drupal.org/user/3317659
